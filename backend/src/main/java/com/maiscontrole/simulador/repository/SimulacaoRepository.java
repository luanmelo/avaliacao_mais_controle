package com.maiscontrole.simulador.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.maiscontrole.simulador.model.Simulacao;

@Repository
public interface SimulacaoRepository extends JpaRepository<Simulacao, Long>  {

}
