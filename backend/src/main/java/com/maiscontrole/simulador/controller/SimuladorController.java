package com.maiscontrole.simulador.controller;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.maiscontrole.simulador.model.Simulacao;
import com.maiscontrole.simulador.model.Usuario;
import com.maiscontrole.simulador.repository.SimulacaoRepository;
import com.maiscontrole.simulador.repository.UsuarioRepository;

@CrossOrigin
@RestController
public class SimuladorController {
	@Autowired
	private UsuarioRepository UsuarioRepo;
	
	@Autowired
	private SimulacaoRepository SimulacaoRepo;

	@Autowired
	  JdbcTemplate jdbcTemplate;

	  class UsuarioMapper implements RowMapper<Usuario> {
		  public Usuario mapRow(ResultSet rs, int arg1) throws SQLException {
			  Usuario usuario = new Usuario();
			  usuario.setId(rs.getLong("id"));
			  usuario.setUsuario(rs.getString("usuario"));
			  usuario.setSenha(rs.getString("senha"));
			  return usuario;
		  }
	  }
	  class SimulacaoMapper implements RowMapper<Simulacao> {
		  public Simulacao mapRow(ResultSet rs, int arg1) throws SQLException {
			  Simulacao simulacao = new Simulacao();
			  simulacao.setId(rs.getLong("id"));
			  simulacao.setId_usuario(rs.getLong("id_usuario"));
			  simulacao.setUnixtime(rs.getLong("unixtime"));
			  simulacao.setValor_entrada(rs.getLong("valor_entrada"));
			  simulacao.setValor_imovel(rs.getLong("valor_imovel"));
			  simulacao.setQtde_parcelas(rs.getLong("qtde_parcelas"));
			  simulacao.setTaxa_juros(rs.getLong("taxa_juros"));
			  return simulacao;
		  }
	  }
	  
    public Usuario validaUsuario(Usuario usuario_teste) {
	    String sql = "select * from usuarios where usuario='" + usuario_teste.getUsuario() + "' and senha='" + usuario_teste.getSenha()
	    + "'";
	    List<Usuario> usuarios = jdbcTemplate.query(sql, new UsuarioMapper());
	    return usuarios.size() > 0 ? usuarios.get(0) : null;
    }

	@RequestMapping(value = "/usuarios")
	public List<Usuario> getUsuarios() {
 
		List<Usuario> listaUsuarios = UsuarioRepo.findAll();
		return listaUsuarios;
	}

	@PostMapping(value = "/add-usuario")
public Usuario addUsuario(@RequestBody Usuario novo_usuario) {
		System.out.println(novo_usuario.getUsuario());
		System.out.println(novo_usuario.getSenha());

	return UsuarioRepo.save(novo_usuario);
}
	@PostMapping(value = "/verifica-usuario")
public Usuario verificaUsuario(@RequestBody Usuario usuario_teste) {
		System.out.println(usuario_teste.getUsuario());
		System.out.println(usuario_teste.getSenha());

	return validaUsuario(usuario_teste);
}

	@PostMapping(value = "/add-simulacao")
public Simulacao addSimulacao(@RequestBody Simulacao nova_simulacao) {
	return SimulacaoRepo.save(nova_simulacao);
}

	@PostMapping(value = "/get-lista-simulacao")
public List<Simulacao> getListaSimulacao(@RequestBody long id_usuario) {
	    String sql = "select * from simulacao where id_usuario='" + id_usuario + "'";
	    return jdbcTemplate.query(sql, new SimulacaoMapper());
}
}
