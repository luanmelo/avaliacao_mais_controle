package com.maiscontrole.simulador.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "simulacao")
public class Simulacao{	

	@Column(name = "id")
	private @Id @GeneratedValue(strategy=GenerationType.IDENTITY) long id;

	@Column(name = "id_usuario")
	private long id_usuario;
	@Column(name = "unixtime")
	private long unixtime;
	@Column(name = "valor_entrada")
	private long valor_entrada;
	@Column(name = "valor_imovel")
	private long valor_imovel;
	@Column(name = "qtde_parcelas")
	private long qtde_parcelas;
	@Column(name = "taxa_juros")
	private long taxa_juros;

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	
	public long getId_usuario() {
		return id_usuario;
	}
	public void setId_usuario(long id_usuario) {
		this.id_usuario = id_usuario;
	}
	public long getUnixtime() {
		return unixtime;
	}
	public void setUnixtime(long unixtime) {
		this.unixtime = unixtime;
	}
	public long getValor_entrada() {
		return valor_entrada;
	}
	public void setValor_entrada(long valor_entrada) {
		this.valor_entrada = valor_entrada;
	}
	public long getValor_imovel() {
		return valor_imovel;
	}
	public void setValor_imovel(long valor_imovel) {
		this.valor_imovel = valor_imovel;
	}
	public long getQtde_parcelas() {
		return qtde_parcelas;
	}
	public void setQtde_parcelas(long qtde_parcelas) {
		this.qtde_parcelas = qtde_parcelas;
	}
	public long getTaxa_juros() {
		return taxa_juros;
	}
	public void setTaxa_juros(long taxa_juros) {
		this.taxa_juros = taxa_juros;
	}
}
