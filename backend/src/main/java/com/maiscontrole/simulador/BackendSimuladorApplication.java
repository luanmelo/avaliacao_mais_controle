package com.maiscontrole.simulador;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BackendSimuladorApplication {

	public static void main(String[] args) {
		SpringApplication.run(BackendSimuladorApplication.class, args);
	}
}
