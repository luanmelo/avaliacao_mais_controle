import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  MatButtonModule, MatCardModule, MatDialogModule, MatInputModule, MatTableModule,
  MatToolbarModule, MatMenuModule,MatIconModule, MatProgressSpinnerModule
} from '@angular/material';

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {FormsModule} from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';

import { RestService } from './rest.service';
import { CadastroUsuarioComponent } from './cadastro-usuario/cadastro-usuario.component';
import { NovaSimulacaoComponent } from './nova-simulacao/nova-simulacao.component';
import { ResultadoSimulacaoComponent } from './resultado-simulacao/resultado-simulacao.component';
import { ListaSimulacaoComponent } from './lista-simulacao/lista-simulacao.component';

const routes: Routes = [
 // { path: '', component: HomeComponent, canActivate: [AuthGuard] },
  { path: '', component: LoginComponent },
  { path: 'login', component: LoginComponent },
  { path: 'cadastro', component: CadastroUsuarioComponent },
  { path: 'home', component: HomeComponent },
  { path: 'nova', component: NovaSimulacaoComponent },
  { path: 'resultado', component: ResultadoSimulacaoComponent },
  { path: 'lista', component: ListaSimulacaoComponent },
  { path: '**', redirectTo: '' }
];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    CadastroUsuarioComponent,
    NovaSimulacaoComponent,
    ResultadoSimulacaoComponent,
    ListaSimulacaoComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule, // new modules added here
    RouterModule.forRoot(routes),
    MatToolbarModule,
    MatCardModule,
    MatButtonModule, 
    MatInputModule, 
    MatDialogModule, 
    MatTableModule, 
    MatMenuModule,
    MatIconModule,
    MatProgressSpinnerModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [
    RestService,
    ],
  bootstrap: [AppComponent]
})
export class AppModule { }
