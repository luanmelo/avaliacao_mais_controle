import { Component, OnInit } from '@angular/core';
import {Usuario} from '../usuario';
import {Router} from '@angular/router';
import { RestService } from '../rest.service';
import { Simulacao } from '../simulacao';

@Component({
  selector: 'abe-lista-simulacao',
  templateUrl: './lista-simulacao.component.html',
  styleUrls: ['./lista-simulacao.component.scss']
})
export class ListaSimulacaoComponent implements OnInit {

  usuarioModel: Usuario;
  simulacoes: any;

  constructor(private router: Router, private restService: RestService) {
    this.usuarioModel = JSON.parse(localStorage.getItem('usuarioAtual'));
    if(this.usuarioModel ==  null) {
      this.router.navigate(["login"]);
    } 
    this.restService.getSimulacao(this.usuarioModel.id).subscribe(res=> {
      console.log(res);
      this.simulacoes = res;
      if(res == null) {
        alert("Não há histórico");
      }
    }, error => alert("Não foi possível receber o histórico de simulações"));
  }

  editaSimulacao(index) {
    console.log(this.simulacoes[index]);
    let myObjStr = JSON.stringify(this.simulacoes[index]);
    this.router.navigate(['resultado', {simulacao: myObjStr, ultimaPagina: 'historico'}]);
  }

  toData(data) {
    return this.formatDateUTCstring(data);
  }
  public formatDateUTCstring(data: string) {
    return this.formatDate(new Date(Number(data)*1000));
  }
  public formatDate(date) {
    let dd = date.getDate();
    let mm = date.getMonth() + 1;
    let yyyy = date.getFullYear();
    if (dd < 10) {dd = '0' + dd; }
    if (mm < 10) {mm = '0' + mm; }
    let hh = date.getHours();
    let MM = date.getMinutes();
    let ss = date.getSeconds();
    if (hh < 10) {hh = '0' + hh; }
    if (MM < 10) {MM = '0' + MM; }
    if (ss < 10) {ss = '0' + ss; }
    date = hh + ':' + mm + ':' + ss + ' - ' + dd + '/' + mm + '/' + yyyy;
    return date;
  }
  ngOnInit() {
  }

}
