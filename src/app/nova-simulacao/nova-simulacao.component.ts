import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import { Simulacao } from '../simulacao';
import {Usuario} from '../usuario';
import { RestService } from '../rest.service';

@Component({
  selector: 'abe-nova-simulacao',
  templateUrl: './nova-simulacao.component.html',
  styleUrls: ['./nova-simulacao.component.scss']
})
export class NovaSimulacaoComponent implements OnInit {

  simulacao: Simulacao;
  usuarioModel: Usuario;
  myObjStr: string;

  constructor(private router: Router, private restService: RestService) { 
    
    this.usuarioModel = JSON.parse(localStorage.getItem('usuarioAtual'));
    if(this.usuarioModel ==  null) {
      this.router.navigate(["login"]);
    }

    this.simulacao = new Simulacao();

    this.simulacao.id_usuario = this.usuarioModel.id;
  }

  ngOnInit() {
  }

  resultado() {
    let time = new Date();
    this.simulacao.unixtime = time.getTime()/1000;
    this.myObjStr = JSON.stringify(this.simulacao);
    this.restService.salvaSimulacao(this.myObjStr).subscribe(res=> {
      console.log(res);
      if(res == null) {
        alert("Não foi possível enviar a simulação");
      } else {
        this.router.navigate(['resultado', {simulacao: this.myObjStr}]);
      }
    }, error => alert("Não foi possível enviar a simulação"));
  }

}
