import { Component, OnInit } from '@angular/core';

import {Router} from '@angular/router';
import {Usuario} from '../usuario';

@Component({
  selector: 'abe-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  usuarioModel: Usuario;

  constructor(private router: Router) {
    this.usuarioModel = JSON.parse(localStorage.getItem('usuarioAtual'));
    console.log(this.usuarioModel);
    if(this.usuarioModel ==  null) {
      this.router.navigate(["login"]);
    }
   }

  ngOnInit() {
  }

}
