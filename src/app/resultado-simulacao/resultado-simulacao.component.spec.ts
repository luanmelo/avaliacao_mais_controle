import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResultadoSimulacaoComponent } from './resultado-simulacao.component';

describe('ResultadoSimulacaoComponent', () => {
  let component: ResultadoSimulacaoComponent;
  let fixture: ComponentFixture<ResultadoSimulacaoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResultadoSimulacaoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResultadoSimulacaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
