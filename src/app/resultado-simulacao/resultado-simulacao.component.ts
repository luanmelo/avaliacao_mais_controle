import { Component, OnInit, LOCALE_ID, Inject  } from '@angular/core';
import { Simulacao } from '../simulacao';
import {Usuario} from '../usuario';
import {ActivatedRoute, Router} from '@angular/router';

class ISimulacao {
  num_parcela: number;
  parcelaSTR: string;
  amortizacaoSTR: string;
  jurosSTR: string;
  saldo_devedorSTR: string;
}

@Component({
  selector: 'abe-resultado-simulacao',
  templateUrl: './resultado-simulacao.component.html',
  styleUrls: ['./resultado-simulacao.component.scss']
})
export class ResultadoSimulacaoComponent implements OnInit {
  back_button = '/home';
  simulacao: Simulacao;
  usuarioModel: Usuario;
  amortizacao: number;
  amortizacaoSTR: number;
  juros: number;
  parcelaSTR: Array<string>;
  saldo_devedor: number;
  num_parcela: Array<number>;

  dataSource: Array<ISimulacao>;
  displayedColumns = [];

  columnNames = [{
    id: "num_parcela",
    value: "Número parcela"

  }, {
    id: "parcelaSTR",
    value: "Parcela"
  },
  {
    id: "amortizacaoSTR",
    value: "Amortização"
  },
  {
    id: "jurosSTR",
    value: "Juros"
  },
  {
    id: "saldo_devedorSTR",
    value: "Saldo devedor"
  }];

  
  constructor(private route: ActivatedRoute, private router: Router, @Inject(LOCALE_ID) public locale: string) { 
    this.displayedColumns = this.columnNames.map(x => x.id);
    
    this.usuarioModel = JSON.parse(localStorage.getItem('usuarioAtual'));
    if(this.usuarioModel ==  null) {
      this.router.navigate(["login"]);
    }
    this.simulacao = JSON.parse(this.route.snapshot.params['simulacao']);
    if(this.route.snapshot.params['ultimaPagina'] == 'historico') {
      this.back_button = '/lista';
    }
    console.log(this.back_button);
    console.log(this.simulacao);
    this.calculaDadosTabela();
   }

   calculaDadosTabela() {
     this.dataSource = new Array<ISimulacao>();
     this.juros = 0;
     this.saldo_devedor = this.simulacao.valor_imovel - this.simulacao.valor_entrada;
     this.amortizacao = this.saldo_devedor/this.simulacao.qtde_parcelas;
     for(let i=1; i<=this.simulacao.qtde_parcelas; i++) {
      let linha_tabela = new ISimulacao();
       linha_tabela.amortizacaoSTR = this.transform(this.amortizacao);
       linha_tabela.num_parcela = i;
       this.juros = (this.simulacao.taxa_juros/100) * this.saldo_devedor;
       linha_tabela.jurosSTR = this.transform(this.juros);
       linha_tabela.parcelaSTR = this.transform(this.amortizacao + this.juros);
       this.saldo_devedor = this.saldo_devedor - this.amortizacao;
       linha_tabela.saldo_devedorSTR = this.transform(this.saldo_devedor);
       this.dataSource.push(linha_tabela);
     }
     console.log(this.dataSource);
   }
   transform(value: number): any {
    return 'R$' + new 
     Intl.NumberFormat('BRL', { style: 'decimal', minimumFractionDigits: 2, maximumFractionDigits: 2 
   }).format(value);
}
   ngOnInit() {
  }
}
