import { Component, OnInit } from '@angular/core';

import {Router} from '@angular/router';
import {MatDialog} from '@angular/material'

import {Usuario} from '../usuario';
import { RestService } from '../rest.service';

@Component({
  selector: 'abe-cadastro-usuario',
  templateUrl: './cadastro-usuario.component.html',
  styleUrls: ['./cadastro-usuario.component.scss']
})
export class CadastroUsuarioComponent implements OnInit {

  constructor(private router: Router, private restService: RestService) { }

  usuario: string;
  senha: string;
  confirma_senha: string;
  usuarioModel: Usuario;

  ngOnInit() {
  }
  login() : void {
    if(this.senha == this.confirma_senha)
    {
      this.usuarioModel = new Usuario();
      this.usuarioModel.usuario = this.usuario;
      this.usuarioModel.senha = this.senha;
      const myObjStr = JSON.stringify(this.usuarioModel);
      console.log(myObjStr);
        this.restService.addUsuario(myObjStr).subscribe(res=> {
          console.log(res);
          alert("Usuário cadastrado com sucesso");
          this.router.navigate(["login"]);
        }, error => alert("Erro ao cadastrar usuário, favor tentar novamente"));
    } else {
      alert("Senha não confere");
    }
  }

}
