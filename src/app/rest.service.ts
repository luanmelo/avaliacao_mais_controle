import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class RestService {

  constructor(private http : HttpClient) { }
    private headers = new HttpHeaders({
      'Content-Type' : 'application/json'
    })

  addUsuario(usuario){
    return this.http.post('http://localhost:8088/add-usuario', usuario, {headers:this.headers});
  }
  verificaLogin(usuario){
    return this.http.post('http://localhost:8088/verifica-usuario', usuario, {headers:this.headers});
  }
  salvaSimulacao(simulacao){
    return this.http.post('http://localhost:8088/add-simulacao', simulacao, {headers:this.headers});
  }
  getSimulacao(id_usuario){
    return this.http.post('http://localhost:8088/get-lista-simulacao', id_usuario, {headers:this.headers});
  }
}
