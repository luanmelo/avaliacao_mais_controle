import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {MatDialog} from '@angular/material'
import {Usuario} from '../usuario';
import { RestService } from '../rest.service';

@Component({
  selector: 'abe-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private router: Router, private restService: RestService) { }

  usuario: string;
  senha: string;
  usuarioModel: Usuario;
  ngOnInit() {
    localStorage.removeItem('usuarioAtual');
  }
  login() : void {
        this.usuarioModel = new Usuario();
        this.usuarioModel.usuario = this.usuario;
        this.usuarioModel.senha = this.senha;
        const myObjStr = JSON.stringify(this.usuarioModel);
          this.restService.verificaLogin(myObjStr).subscribe(res=> {
            console.log(res);
            if(res == null) {
              alert("Não foi possível efetuar o login");
            } else {
              localStorage.setItem('usuarioAtual', JSON.stringify(res));
              this.router.navigate(["home"]);
            }
          }, error => alert("Não foi possível efetuar o login"));
  }
  cadastrar() : void {
    this.router.navigate(["cadastro"]);
  }
}
